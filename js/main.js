$(document).ready(function(){
    // .modal-btn
    $('.modal-btn').click(function (e) {
        e.preventDefault();

        var modal = $(this).attr('href');

        $('.modal').removeClass('modal--active');
        $(modal).addClass('modal--active');
        $('body').addClass('body-hidden');

        if($('#done').is('.modal--active')) {
            setTimeout(function () {
                $('#done').removeClass('modal--active');
                $('body').removeClass('body-hidden');
            },5000);
        }
    });

    $('.modal__close').click(function (e) {
        e.preventDefault();

        if(!$(this).hasClass('modal-btn')) {
            $('.modal').removeClass('modal--active');
            $('body').removeClass('body-hidden');
        }
    });

    $('.modal').on('click', function(e) {
        if (e.target !== this)
            return;

        $('.modal').removeClass('modal--active');
        $('body').removeClass('body-hidden');
    });
});